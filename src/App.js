import logo from './logo.svg';
import './App.css';
import React, { useEffect, useState } from "react";
import ReactDOM from 'react-dom';
import Web3 from 'web3';
import {Biconomy} from "@biconomy/mexa";
import { abi } from "./abi";
import { abi_mainnnet } from './abi_mainnet';
import {ethers} from 'ethers'
import {toBuffer} from "ethereumjs-util";
import axios from 'axios';
let sigUtil = require("eth-sig-util");
let eth_abi = require('ethereumjs-abi')

const { createAlchemyWeb3 } = require("@alch/alchemy-web3");
let contract
let newQuote = 0
let message = {};
let helperAttributes = {};
helperAttributes.baseURL = "https://api.biconomy.io";
let nonce = '';
let gasPrice = '';
function App() {
 
  
    
    // const contract_address = "0x98645742e49cbd9804e40e301192a1b21fc11be8"
    // const contract_address = "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174"
    // let maticProvider = new Web3.providers.HttpProvider("https://rpc-mumbai.matic.today");
    // let maticProvider = new Web3.providers.HttpProvider("https://rpc-mumbai.maticvigil.com/");
    // const maticProvider = createAlchemyWeb3("https://polygon-mumbai.g.alchemy.com/v2/WjeC7u7rHoS6hM1ub7FpZdhU7DMf-iup");
    // const biconomy = new Biconomy(maticProvider,{apiKey:"NHhIp14Ob.8bc44a3d-362e-4402-a60f-8dee771eee1b"}); 
    
    const userAddress = '0x6a8a201de420fDb0305fd67285aa8477F28dd396';
    const contract_address = "0xe6b8a5CF854791412c1f6EFC7CAf629f5Df1c747"
    const mainnet_contract_address = "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174"
    const privateKey = "asdafr........"
    const rpc_key_mainnet ="https://polygon-mainnet.g.alchemy.com/v2/g2eh_HAncn98Bd--DuOBQ_QxdAQLr0Fe" 
    const rpc_key_mumbai = "https://polygon-mumbai.g.alchemy.com/v2/WjeC7u7rHoS6hM1ub7FpZdhU7DMf-iup"
    const biconomy_key_mainnet = "LoGSK5e40.a7686bbd-31af-40cd-92cb-aa41a2e20b98"
    const biconomy_key_mumbai = "D4FEmnbuW.0852acf6-d5f2-48b1-bf8c-c25d1c7a508a"
    // const contract_address = "0xDD9185DB084f5C4fFf3b4f70E7bA62123b812226"
    // const contract_address = "0xD27d50560E4026c3009AE440F482473454C96a9B"
    // const contract_address = "0xcB1e72786A6eb3b44C2a2429e317c8a2462CFeb1"
    let maticProvider = new Web3.providers.HttpProvider(rpc_key_mainnet);
    // const biconomy = new Biconomy(maticProvider,{apiKey:"pnS-Q0LAa.b0a82c53-07ab-4596-b386-96e4ea55bdf5",debug:true}); 
    const biconomy = new Biconomy(maticProvider,{apiKey:biconomy_key_mainnet,debug:true}); 
    const web3 = new Web3(biconomy);
    const webeth3 = new Web3(window.ethereum);
     
    const  walletWeb3 = new Web3(window.ethereum) //to sign message
   
    const domainType = [
      { name: "name", type: "string" },
      { name: "version", type: "string" },
      { name: "verifyingContract", type: "address" },
      { name: "salt", type: "bytes32" },
    ];
    const metaTransactionType = [
      { name: "nonce", type: "uint256" },
      { name: "from", type: "address" },
      { name: "functionSignature", type: "bytes" }
    ];
    // mumbai
    // let domainData = {
    //   // name: "TestContract",
    //   name: " Mumbai USD Coin",
    //   version: "1",
    //   verifyingContract: contract_address,
    //   // converts Number to bytes32. Use your chainId instead of 42 if network is not Kovan
    //   salt: '0x' + (80001).toString(16).padStart(64, '0')
    // }
    // mainnet
    let domainData = {
      // name: "TestContract",
      name: "USD Coin",
      version: "1",
      verifyingContract: mainnet_contract_address,
      // converts Number to bytes32. Use your chainId instead of 42 if network is not Kovan
      salt: '0x' + (137).toString(16).padStart(64, '0')
    }
    
    
           
    biconomy.onEvent(biconomy.READY, async () => {
        // Initialize your dapp here like getting user accounts etc
        await window.ethereum.enable();

        //mumbai

        //  contract = new web3.eth.Contract( 
        // //  contract = new webeth3.eth.Contract(
        //   abi,
        //   contract_address,
        //   // biconomy.getSignerByAddress(userAddress)
        // );

        // mainnet 

        contract = new web3.eth.Contract(
          //  contract = new webeth3.eth.Contract(
            abi_mainnnet,
            mainnet_contract_address,
            // biconomy.getSignerByAddress(userAddress)
          );

        console.log('contract',contract);
     
        
    
      }).onEvent(biconomy.ERROR, (error, message) => {
        // Handle error while initializing mexa
        console.log('error',error)
        console.log('message',message)
      });
      
      async function transferToken(){
        // let contract = await Web3.eth.contract(abi).at(contract_address);
        let contract =  new webeth3.eth.Contract(abi, contract_address);
        console.log('contact',contract)
        let result = await contract.methods.transfer("0x01A901da6c490FF7FeD67f2f5Dedc5A74088FB51", "100000000").send({from:userAddress})
        console.log('result',result)
        
      }
    
      async function getNonce(){
        console.log('contract',contract)
        try{
           nonce =  await contract.methods.nonces(userAddress).call()
          
          // let nonce = await contract.methods.balanceOf(contract_address).call();

          
  
          console.log('nonce',nonce);
        }catch(e){
          console.log('error',e);
        }
      
        
      }
      async function balance(){
        console.log('contract',contract)
        try{
          let balance =  await contract.methods.balanceOf(userAddress).call()
          // let nonce = await contract.methods.balanceOf(contract_address).call();

          
  
          console.log('balance',balance);
        }catch(e){
          console.log('error',e);
        }
      
        
      }
      async function  transaction() {
        try{
          let functionSignature =  await contract.methods.transfer("0x01A901da6c490FF7FeD67f2f5Dedc5A74088FB51","1000000").encodeABI();
        
          console.log('fun-sig', functionSignature)

          message.nonce = parseInt(nonce);
          message.from = userAddress;
          message.functionSignature = functionSignature;
          console.log( 'message',message);
          const nameContract = await contract.methods.name().call()
          domainType.name = nameContract;          // message.gas = '10000000000';
          // "message":{"from":{"name":"Cow","wallet":"0xCD2a3d9F938E13CD947Ec05AbC7FE734Df8DD826"},"to":{"name":"Bob","wallet":"0xbBbBBBBbbBBBbbbBbbBbbbbBBbBbbbbBbBbbBBbB"},"contents":"Hello, Bob!"}}],"id":1}'
          const dataToSign = JSON.stringify({
            types: {
              EIP712Domain: domainType,
              MetaTransaction: metaTransactionType
            },
            domain: domainData,
            primaryType: "MetaTransaction",
            message: message
          });

          // sign
          walletWeb3.currentProvider.send({
            jsonrpc: "2.0",
            id: 999999999999,
            method: "eth_signTypedData_v4",
            params: [userAddress, dataToSign]
          },async function(error, result) {
            console.log('response',result)
            
            
            console.log('error',error)
            
            // Check github repository for getSignatureParameters helper method
            let { r, s, v } = getSignatureParameters(result.result);
              let {gasLimit,gasPrice} = await sendSignedTransaction(userAddress, functionSignature, r, s, v);
           
            let tx = await  contract.methods.executeMetaTransaction(userAddress,
            functionSignature, r, s, v)
            //  .estimateGas({ from: userAddress });
            .send({from: userAddress});
            // .encodeABI()

            console.log('tx',tx)


          //   let txParams = {
          //     "from": userAddress,
          //     "to": "0x01A901da6c490FF7FeD67f2f5Dedc5A74088FB51",
          //     "value": "0x1000000",
          //     "gas": "500000", // (optional) your custom gas limit 
          //     "data": tx
          // }
          // const res = await walletWeb3.eth.sendTransaction(txParams);
          // console.log('txParams',res)
            // .send({from: userAddress,gasPrice :'10000000000',gas:10000000000,gasLimit:'10000000000',value:'10000',to:'0x01A901da6c490FF7FeD67f2f5Dedc5A74088FB51'});
        
            // tx.on("transactionHash", function(hash) {
            //   // Handle transaction hash
            //   console.log('txhash',hash)
            // }).once("confirmation", function(confirmationNumber, receipt) {
            //   // Handle confirmation
            //   console.log('confirmationNumber',confirmationNumber)
            //   console.log('reciept',receipt)
            // }).on("error", function(error) {
            //   // Handle error
            //   console.log('tx error',error)
            // });
            
          }
        );


        }catch(e){
          console.log('trans error: ',e)
        }
      }
      const sendSignedTransaction = async (userAddress, functionData, r, s, v) => {
        // if (web3 && contract) {
          try {
         let gasLimit = '4000000';
            // let gasLimit = await contract.methods
            //   .executeMetaTransaction(userAddress, functionData, r, s, v)
            //   .estimateGas({ from: userAddress });
            let gasPrice = await web3.eth.getGasPrice();
            console.log('gasLimit',gasLimit);
            console.log('gasPrice',gasPrice);
             return {
              gasLimit,
              gasPrice

            }
        
//             var amount = 1000
// // var tokens = web3.utils.toWei(amount.toString(), 'ether')
// //             let tx = contract.methods
// //               .executeMetaTransaction(userAddress, functionData, r, s, v)
// //               .send({
// //                 from: userAddress,
// //                 gasPrice:gasPrice,
// //                 gasLimit:gasLimit,
               
// //               });
    
            // tx.on("transactionHash", function(hash) {
            //   console.log(`Transaction hash is ${hash}`);
            //   // showInfoMessage(`Transaction sent by relayer with hash ${hash}`);
            // }).once("confirmation", function(confirmationNumber, receipt) {
            //   console.log(receipt);
            //   // setTransactionHash(receipt.transactionHash);
            //   // showSuccessMessage("Transaction confirmed on chain");
            //   // getQuoteFromNetwork();
            // });
          } catch (error) {
            console.log('gasError',error);
          }
        // }
      };
      const personalSign = async () =>{
        let functionSignature =  await contract.methods.transfer("0x01A901da6c490FF7FeD67f2f5Dedc5A74088FB51","1000000").encodeABI();
        let messageToSign = constructMetaTransactionMessage(nonce,'80001', functionSignature, contract_address);
        let {signature} = webeth3.eth.accounts.sign("0x" + messageToSign.toString("hex"), privateKey);
        let { r, s, v } = getSignatureParameters(signature);
        let executeMetaTransactionData = contract.methods.executeMetaTransaction(userAddress, functionSignature, r, s, v).encodeABI();
                let txParams = {
                    "from": userAddress,
                    "to": contract_address,
                    "value": "0x0",
                    "gas": "100000",
                    "data": executeMetaTransactionData
                };
                const signedTx = await webeth3.eth.accounts.signTransaction(txParams, `0x${privateKey}`);
                let receipt = await webeth3.eth.sendSignedTransaction(signedTx.rawTransaction, (error, txHash) => {
                    if (error) {
                        return console.error('receipt error',error);
                    }
                    console.log("Transaction hash is ", txHash);
                    // showInfoMessage(`Transaction sent to blockchain with hash ${txHash}`);
                });        
      }
      const getGasPrice = async () => {
       let networkId = 80001;
        const apiInfo = `${
            helperAttributes.baseURL
        }/api/v1/gas-price?networkId=${networkId}`;
        axios(apiInfo).then(  (response) =>{
        
          // const responseJson = await response.json();
          gasPrice = ethers.utils.parseUnits(response.data.gasPrice.value.toString(), "gwei").toString();
          console.log("Response JSON " + gasPrice);
        }).catch(error =>{
          console.log('gasprice erro :',error)
        })
        // const response = await fetch(apiInfo);
        // const responseJson = await response.json();
        // console.log("Response JSON " + JSON.stringify(responseJson));
        
      };
      const constructMetaTransactionMessage = (nonce, chainId, functionSignature, contractAddress) => {
        return eth_abi.soliditySHA3(
            ["uint256","address","uint256","bytes"],
            [nonce, contractAddress, chainId, toBuffer(functionSignature)]
        );
      }
      const getSignatureParameters = signature => {
        if (!web3.utils.isHexStrict(signature)) {
          throw new Error(
            'Given value "'.concat(signature, '" is not a valid hex string.')
          );
        }
        var r = signature.slice(0, 66);
        var s = "0x".concat(signature.slice(66, 130));
        var v = "0x".concat(signature.slice(130, 132));
        v = web3.utils.hexToNumber(v);
        if (![27, 28].includes(v)) v += 27;
        return {
          r: r,
          s: s,
          v: v
        };
      };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          {/* Edit <code>src/App.js</code> and save to reload. */}
        </p>
       
        <button onClick={balance}>balance</button>
        <button onClick={getGasPrice}>getGas</button>
        <button onClick={getNonce}>GetNonce</button>
        <button onClick={transaction}>TransferWithBiconomy</button>
        <button onClick={personalSign}>PersonalSignREquest</button>
        {/* <button onClick={transferToken}>TransferWithoutBiconomy</button> */}
      </header>
    </div>
  );
}

export default App;
